<%-- 
    Document   : crearUsuario
    Created on : Apr 22, 2019, 4:28:34 PM
    Author     : Guillermo
    Obs        : El usuario al ser creado tendrá el atributo activo por defecto 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Creación de Nuevo Usuario</title>
    </head>
    <body>
        <form action="" method="get">
          <p>Nombre: <input type="text" name="nombre" size="40"></p>
          <p>Apellido: <input type="text" name="apellido" size="40"></p>
          <p>Email: <input type="text" name="email" size="40"></p>
          <p>Dirección: <input type="text" name="direccion" size="60"></p>
          <p>Teléfono: <input type="text" name="telefono" size="11"></p>
          <p>Usuario: <input type="text" name="username" size="12"></p>
          <p>Contraseña: <input type="text" name="password" size="12"></p>
          
          <p>
            <input type="submit" value="Crear">
            <input type="reset" value="Borrar">
          </p>
        </form>
    </body>
</html>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Guillermo
 */
@Entity
@Table(name = "sprint_backlog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SprintBacklog.findAll", query = "SELECT s FROM SprintBacklog s")
    , @NamedQuery(name = "SprintBacklog.findByIdTarea", query = "SELECT s FROM SprintBacklog s WHERE s.sprintBacklogPK.idTarea = :idTarea")
    , @NamedQuery(name = "SprintBacklog.findByIdSprint", query = "SELECT s FROM SprintBacklog s WHERE s.sprintBacklogPK.idSprint = :idSprint")
    , @NamedQuery(name = "SprintBacklog.findByDescripcion", query = "SELECT s FROM SprintBacklog s WHERE s.descripcion = :descripcion")
    , @NamedQuery(name = "SprintBacklog.findByEstado", query = "SELECT s FROM SprintBacklog s WHERE s.estado = :estado")
    , @NamedQuery(name = "SprintBacklog.findByFechaFin", query = "SELECT s FROM SprintBacklog s WHERE s.fechaFin = :fechaFin")
    , @NamedQuery(name = "SprintBacklog.findByFechaVencimiento", query = "SELECT s FROM SprintBacklog s WHERE s.fechaVencimiento = :fechaVencimiento")
    , @NamedQuery(name = "SprintBacklog.findByActivo", query = "SELECT s FROM SprintBacklog s WHERE s.activo = :activo")})
public class SprintBacklog implements Serializable {

    @Column(name = "fecha_ini")
    @Temporal(TemporalType.DATE)
    private Date fechaIni;
    @JoinColumn(name = "id_prioridad", referencedColumnName = "id_prioridad")
    @ManyToOne
    private Prioridades idPrioridad;
    @JoinColumn(name = "id_history", referencedColumnName = "id_history", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ProductBacklog productBacklog;

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SprintBacklogPK sprintBacklogPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "estado")
    private String estado;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "activo")
    private String activo;
    @JoinColumn(name = "id_sprint", referencedColumnName = "id_sprint", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sprints sprints;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idUsuario;

    public SprintBacklog() {
    }

    public SprintBacklog(SprintBacklogPK sprintBacklogPK) {
        this.sprintBacklogPK = sprintBacklogPK;
    }

    public SprintBacklog(SprintBacklogPK sprintBacklogPK, String descripcion, String estado, Date fechaVencimiento, String activo) {
        this.sprintBacklogPK = sprintBacklogPK;
        this.descripcion = descripcion;
        this.estado = estado;
        this.fechaVencimiento = fechaVencimiento;
        this.activo = activo;
    }

    public SprintBacklog(int idTarea, int idSprint) {
        this.sprintBacklogPK = new SprintBacklogPK(idTarea, idSprint);
    }

    public SprintBacklogPK getSprintBacklogPK() {
        return sprintBacklogPK;
    }

    public void setSprintBacklogPK(SprintBacklogPK sprintBacklogPK) {
        this.sprintBacklogPK = sprintBacklogPK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Sprints getSprints() {
        return sprints;
    }

    public void setSprints(Sprints sprints) {
        this.sprints = sprints;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sprintBacklogPK != null ? sprintBacklogPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SprintBacklog)) {
            return false;
        }
        SprintBacklog other = (SprintBacklog) object;
        if ((this.sprintBacklogPK == null && other.sprintBacklogPK != null) || (this.sprintBacklogPK != null && !this.sprintBacklogPK.equals(other.sprintBacklogPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.SprintBacklog[ sprintBacklogPK=" + sprintBacklogPK + " ]";
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Prioridades getIdPrioridad() {
        return idPrioridad;
    }

    public void setIdPrioridad(Prioridades idPrioridad) {
        this.idPrioridad = idPrioridad;
    }

    public ProductBacklog getProductBacklog() {
        return productBacklog;
    }

    public void setProductBacklog(ProductBacklog productBacklog) {
        this.productBacklog = productBacklog;
    }
    
}

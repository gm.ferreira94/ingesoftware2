/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Guillermo
 */
@Entity
@Table(name = "product_backlog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductBacklog.findAll", query = "SELECT p FROM ProductBacklog p")
    , @NamedQuery(name = "ProductBacklog.findByIdHistory", query = "SELECT p FROM ProductBacklog p WHERE p.idHistory = :idHistory")
    , @NamedQuery(name = "ProductBacklog.findByDescripcion", query = "SELECT p FROM ProductBacklog p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "ProductBacklog.findByEstado", query = "SELECT p FROM ProductBacklog p WHERE p.estado = :estado")
    , @NamedQuery(name = "ProductBacklog.findByActivo", query = "SELECT p FROM ProductBacklog p WHERE p.activo = :activo")})
public class ProductBacklog implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productBacklog")
    private Collection<SprintBacklog> sprintBacklogCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_history")
    private Integer idHistory;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "activo")
    private String activo;
    @JoinColumn(name = "id_prioridad", referencedColumnName = "id_prioridad")
    @ManyToOne(optional = false)
    private Prioridades idPrioridad;
    @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto")
    @ManyToOne(optional = false)
    private Proyectos idProyecto;

    public ProductBacklog() {
    }

    public ProductBacklog(Integer idHistory) {
        this.idHistory = idHistory;
    }

    public ProductBacklog(Integer idHistory, String descripcion, String estado, String activo) {
        this.idHistory = idHistory;
        this.descripcion = descripcion;
        this.estado = estado;
        this.activo = activo;
    }

    public Integer getIdHistory() {
        return idHistory;
    }

    public void setIdHistory(Integer idHistory) {
        this.idHistory = idHistory;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Prioridades getIdPrioridad() {
        return idPrioridad;
    }

    public void setIdPrioridad(Prioridades idPrioridad) {
        this.idPrioridad = idPrioridad;
    }

    public Proyectos getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Proyectos idProyecto) {
        this.idProyecto = idProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHistory != null ? idHistory.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductBacklog)) {
            return false;
        }
        ProductBacklog other = (ProductBacklog) object;
        if ((this.idHistory == null && other.idHistory != null) || (this.idHistory != null && !this.idHistory.equals(other.idHistory))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.ProductBacklog[ idHistory=" + idHistory + " ]";
    }

    @XmlTransient
    public Collection<SprintBacklog> getSprintBacklogCollection() {
        return sprintBacklogCollection;
    }

    public void setSprintBacklogCollection(Collection<SprintBacklog> sprintBacklogCollection) {
        this.sprintBacklogCollection = sprintBacklogCollection;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Guillermo
 */
@Embeddable
public class SprintBacklogPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tarea")
    private int idTarea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_sprint")
    private int idSprint;

    public SprintBacklogPK() {
    }

    public SprintBacklogPK(int idTarea, int idSprint) {
        this.idTarea = idTarea;
        this.idSprint = idSprint;
    }

    public int getIdTarea() {
        return idTarea;
    }

    public void setIdTarea(int idTarea) {
        this.idTarea = idTarea;
    }

    public int getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(int idSprint) {
        this.idSprint = idSprint;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idTarea;
        hash += (int) idSprint;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SprintBacklogPK)) {
            return false;
        }
        SprintBacklogPK other = (SprintBacklogPK) object;
        if (this.idTarea != other.idTarea) {
            return false;
        }
        if (this.idSprint != other.idSprint) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.SprintBacklogPK[ idTarea=" + idTarea + ", idSprint=" + idSprint + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Guillermo
 */
@Entity
@Table(name = "prioridades")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prioridades.findAll", query = "SELECT p FROM Prioridades p")
    , @NamedQuery(name = "Prioridades.findByIdPrioridad", query = "SELECT p FROM Prioridades p WHERE p.idPrioridad = :idPrioridad")
    , @NamedQuery(name = "Prioridades.findByDescripcion", query = "SELECT p FROM Prioridades p WHERE p.descripcion = :descripcion")})
public class Prioridades implements Serializable {

    @OneToMany(mappedBy = "idPrioridad")
    private Collection<SprintBacklog> sprintBacklogCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_prioridad")
    private Integer idPrioridad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPrioridad")
    private Collection<ProductBacklog> productBacklogCollection;

    public Prioridades() {
    }

    public Prioridades(Integer idPrioridad) {
        this.idPrioridad = idPrioridad;
    }

    public Prioridades(Integer idPrioridad, String descripcion) {
        this.idPrioridad = idPrioridad;
        this.descripcion = descripcion;
    }

    public Integer getIdPrioridad() {
        return idPrioridad;
    }

    public void setIdPrioridad(Integer idPrioridad) {
        this.idPrioridad = idPrioridad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<ProductBacklog> getProductBacklogCollection() {
        return productBacklogCollection;
    }

    public void setProductBacklogCollection(Collection<ProductBacklog> productBacklogCollection) {
        this.productBacklogCollection = productBacklogCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPrioridad != null ? idPrioridad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prioridades)) {
            return false;
        }
        Prioridades other = (Prioridades) object;
        if ((this.idPrioridad == null && other.idPrioridad != null) || (this.idPrioridad != null && !this.idPrioridad.equals(other.idPrioridad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Prioridades[ idPrioridad=" + idPrioridad + " ]";
    }

    @XmlTransient
    public Collection<SprintBacklog> getSprintBacklogCollection() {
        return sprintBacklogCollection;
    }

    public void setSprintBacklogCollection(Collection<SprintBacklog> sprintBacklogCollection) {
        this.sprintBacklogCollection = sprintBacklogCollection;
    }
    
}
